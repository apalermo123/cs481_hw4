import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

void main() {
  runApp(MyApp());
}

class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween = Tween<double>(begin: 0.1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 300);


  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {

    final animation = listenable as Animation<double>;
    return Center(
      child: SingleChildScrollView(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 50),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          //child: FlutterLogo(),
          child: new SingleChildScrollView(
          padding: EdgeInsets.all(40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
                Image.asset('nuke.jpg'),
                Text ('WARNING', textDirection: TextDirection.ltr,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.red,
                  fontSize: 40.0),
                ),
            ]
        ),
          ),
          //child: Text('Warning'),
        ),
      ),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  _LogoState createState() => _LogoState();
}

class _LogoState extends State<MyApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;
  int counter = 0;
  //AudioPlayer audioPlayer = new AudioPlayer();

  @override
  void initState() {
    super.initState();
    //playRemoteFile();
    playLocalAsset();
    //audioPlayer.play("https://www.youtube.com/watch?v=7Qtst6BHZ3s&ab_channel=SFXandGFX");
    controller = AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
      if (counter <= 4) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
          counter++;
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      }
        });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  Future<AudioPlayer> playLocalAsset() async {
    AudioCache cache = new AudioCache();
    return await cache.play("nukesound.mp3");
  }

  //void playRemoteFile() {
    //AudioPlayer player = new AudioPlayer();
    //player.play("https://www.youtube.com/watch?v=7Qtst6BHZ3s&ab_channel=SFXandGFX");
  //}

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
